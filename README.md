# Omolete

É um pacote com o objectivo de reduzir o esforço do dev com configurações de banco, jwt, etc e focar-se no negocio da aplicação...

# Como usar

```bash
npm i sugar
```

```js
const createConnection = require("createConnection");

// createConnection(driver)
const connect = createConnection("mysql");
```
